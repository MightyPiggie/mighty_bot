const fs = require('fs');
const path = require('path');

const registerCommand = require('./commands.js').registerCommand;
const getUUID = require('./uuidTracker.js').getUUID;
const ciCompare = require('../lib/ciCompare.js');

module.exports.inject = inject;

/**
 * Class for the coordinates of someones base.
 */
class BaseCoordinates {
  /**
   * Constructor for the BaseCoordinates class.
   *
   * @param {int} x - The overworld x coordinate.
   * @param {int} z - The overworld z coordinate.
   */
  constructor(x, z) {
    this.x = x;
    this.z = z;
    this.nether_x = Math.round(x/8);
    this.nether_z = Math.round(z/8);
  }
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  let baseLocations;
  const baseLocationsFile = path.join(__dirname, '../data', 'baseLocations.json');
  if (!fs.existsSync(path.join(__dirname, '../data'))) {
    fs.mkdir(path.join(__dirname, '../data'), (err) => {
      if (err) {
        log.error(err);
        throw err;
      }
    });
  }
  if (fs.existsSync(baseLocationsFile)) {
    baseLocations = JSON.parse(fs.readFileSync(baseLocationsFile));
  } else {
    baseLocations = {};
  }

  /**
   * Say the location of someones base if it exists.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function findBase(username, args, response) {
    if (ciCompare(args[0], 'is')) {
      args.shift();
    }
    const uuid = getUUID(args[0]);
    if (Object.keys(baseLocations).includes(uuid)) {
      const base = baseLocations[uuid];
      const direction = Math.abs(base.x) > Math.abs(base.z) ? (base.x > 0 ? 'east' : 'west') : (base.z > 0 ? 'south' : 'north');
      response(`${args[0]}'s base is at (${base.x},${base.z}), (${base.nether_x},${base.nether_z}) in the nether, ${direction} tunnel.`);
      log.debug(`${username} found the base of ${args[0]}`);
    } else {
      response(`I can't find the base of ${args[0]} in my database :(`);
      log.debug(`${args[0]} isn't in the database.`);
    }
  }

  /**
   * Add location of someones base to the database.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function setBase(username, args, response) {
    const x = parseInt(args[0]);
    if (isNaN(x)) {
      response(`${args[0]} is not a whole number.`);
      return;
    }
    const z = parseInt(args[1]);
    if (isNaN(z)) {
      response(`${args[1]} is not a whole number.`);
      return;
    }
    baseLocations[getUUID(username)] = new BaseCoordinates(x, z);
    fs.writeFile(baseLocationsFile, JSON.stringify(baseLocations, null, 4), (err) => {
      if (err) {
        log.err(err);
        throw err;
      }
    });
    response('Saved the location.');
    log.debug(`${username} saved his base location at (${x}, ${z})`);
  }

  registerCommand('where', findBase, 'Find someones base', 'where [\'is\'] username', 1);
  registerCommand('setbase', setBase, 'Set your base location', 'setbase x_coordinate z_coordinate', 2);
};
