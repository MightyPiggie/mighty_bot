const registerCommand = require('./commands.js').registerCommand;
const ciCompare = require('../lib/ciCompare.js');

module.exports = {
  inject: inject,
  options: {
    masters: [],
    commandWhitelist: [],
  },
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  bot.checkPermission = (username, command, source) => bot.options.masters.includes(username) || (bot.options.commandWhitelist.includes(command));

  /**
   * Manage the permissions of the bot.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function managePermission(username, args, response) {
    if (!ciCompare(args[0], 'add') && !ciCompare(args[0], 'remove')) {
      response(`${args[0]} is not a command I recognize.`);
      return;
    }
    if (ciCompare(args[0], 'add')) {
      if (bot.options.masters.includes(args[0])) {
        response(`${args[1]} is already one of my masters.`);
      } else {
        bot.options.masters.push(args[1]);
        log.info(`${username} added ${args[1]} to the masters list.`);
        response(`${args[1]} is added to the masters list`);
      }
    } else if (ciCompare(args[0], 'remove')) {
      const index = bot.options.masters.indexOf(args[1]);
      if (index == -1) {
        response(`${args[1]} isn't one of my masters.`);
      } else {
        bot.options.masters.splice(index, 1);
        log.info(`${username} removed ${args[1]} from the masters list.`);
        response(`${args[1]} is removed from the masters list`);
      }
    }
  }

  registerCommand('master', managePermission, 'Manage the access to the bot', 'master add/remove username', 2);
  registerCommand('masters', (username, args, response) => response(bot.options.masters.join(', ')), 'Get a list of masters', 'masters');
};
