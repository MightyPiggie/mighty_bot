const mathjs = require('mathjs');

const registerCommand = require('./commands.js').registerCommand;

module.exports = {
  inject: inject,
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  const scopes = {global: {}};

  /**
   * Calculate a math expression.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function calculate(username, args, response) {
    const scope = response.source == 'chat' ? 'global' : username;

    if (args[0] == 'clear') {
      scopes[scope] = {};
      response('The variables have been cleared.');
      log.debug(`Cleared the variables in scope ${scope}`);
    } else {
      try {
        if (response.source == 'whisper') {
          if (!(username in scopes)) {
            scopes[username] = {};
          }
        }
        response(`Result: ${mathjs.evaluate(args.join(' '), scopes[scope])}`);
        log.debug(`Calculated the expression '${args.join(' ')}' in scope ${scope}`);
      } catch (e) {
        response(`Error: ${e.message}`);
        log.debug(`Received invallid math expression '${args.join(' ')}' in scope ${scope}`);
      }
    }
  }

  registerCommand('calc', calculate, 'Calculate a math expression.', 'calc expression', 1);
};
