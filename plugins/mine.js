const registerCommand = require('./commands.js').registerCommand;
const ciCompare = require('../lib/ciCompare.js');

module.exports = {
  inject: inject,
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  const mcData = require('minecraft-data')(bot.version);
  let mineInterval;
  let target = '';
  let lastResponse;

  /**
   * Equip another tool with the same name and durability above 10
   *
   * @param {Item} tool - The tool to find a replacement for
   * @return {bool} Whether a replacement was found
   */
  function equipOtherTool(tool) {
    for (let i = bot.inventory.inventoryStart; i < bot.inventory.inventoryEnd; ++i) {
      const item = bot.inventory.slots[i];
      if (item?.name === tool.name) {
        const durabilityLeft = mcData.itemsByName[item.name].maxDurability - item.durabilityUsed;
        if (durabilityLeft >= 10) {
          bot.equip(item, 'hand');
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Check the durability of the current tool.
   * If the durability is below 10, check if another tool is available.
   *
   * @return {bool} Whether everything is good to continue
   */
  function checkDurability() {
    const tool = bot.heldItem;
    const maxDurability = mcData.itemsByName[tool?.name]?.maxDurability;
    if (maxDurability - tool?.durabilityUsed < 10) {
      return equipOtherTool(tool);
    }
    return true;
  }

  /**
   * Async mine a block
   */
  function mine() {
    if (bot.targetDigBlock === null) {
      // Check if tools are ok
      if (!checkDurability()) {
        lastResponse(`I ran out of tools while mining ${target}`);
        clearInterval(mineInterval);
        return;
      }

      const block = bot.blockAtCursor(maxDistance=2);
      if (block?.name === target) {
        bot.dig(block);
      }
    }
  }

  /**
   * Mine block infront of you
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function mineCommand(username, args, response) {
    if (args.length != 0 && (ciCompare(args[0], 'stop'))) {
      clearInterval(mineInterval);
      response('Canceled mine');
    } else if (args.length != 0) {
      target = args[0];
      mineInterval = setInterval(mine, 1000);
      response(`Started mining ${target}`);
      lastResponse = response;
    }
  }
  registerCommand('mine', mineCommand, 'Mine blocks.', 'block to be mined', 1);
};
