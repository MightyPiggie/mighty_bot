const registerCommand = require('./commands.js').registerCommand;

module.exports = {
  inject: inject,
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Check current Minecraf time.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function checkTime(username, args, response) {
    response(`It's ${(Math.floor(bot.time.timeOfDay / 1000) + 6) % 24}:${Math.floor(60 * ((bot.time.timeOfDay / 1000) % 1))} of day ${bot.time.day}. Full moon is ${bot.time.moonPhase == 0 ? 'tonight' : `in ${8 - bot.time.moonPhase} ${bot.time.moonPhase == 7 ? 'day' : 'days'}`}.`);
  }

  registerCommand('time', checkTime, 'See current time and moon-phase', 'time');
};
