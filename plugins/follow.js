const vec3 = require('vec3');

const registerCommand = require('./commands.js').registerCommand;
const ciCompare = require('../lib/ciCompare.js');

module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  let followInterval;

  /**
   * Follow a player.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function follow(username, args, response) {
    if (args.length != 0 && (ciCompare(args[0], 'stop') || ciCompare(args[0], 'nobody'))) {
      clearInterval(followInterval);
      bot.setControlState('forward', false);
      log.debug('The bot stopped following.');
      return;
    }
    const nearbyPlayers = Object.values(bot.entities).filter((entity) => entity.type == 'player').map((player) => player.username);
    const targetName = (args.length == 0 || ciCompare(args[0], 'me')) ? username : args[0];

    if (!nearbyPlayers.includes(targetName)) {
      response(`I tried to follow ${targetName} but I couldn't see him.`);
      return;
    }

    const target = bot.players[targetName];
    clearInterval(followInterval);
    followInterval = setInterval(() => {
      if (target.entity == undefined) {
        response(`I didn't see ${targetName} anymore, so I stopped following.`);
        clearInterval(followInterval);
        return;
      }
      bot.lookAt(target.entity.position.plus(vec3(0, 1.62, 0)));
      if (bot.entity.position.distanceTo(target.entity.position) > 1) {
        bot.setControlState('forward', true);
      } else {
        bot.setControlState('forward', false);
      }
      // Jump if there is a solid block near.
      if (bot.blockAt(bot.entity.position.plus(vec3(1, 0, 0)))
          .boundingBox == 'block' ||
                bot.blockAt(bot.entity.position.plus(vec3(-1, 0, 0)))
                    .boundingBox == 'block' ||
                bot.blockAt(bot.entity.position.plus(vec3(0, 0, 1)))
                    .boundingBox == 'block' ||
                bot.blockAt(bot.entity.position.plus(vec3(0, 0, -1)))
                    .boundingBox == 'block') {
        bot.setControlState('jump', true);
        bot.setControlState('jump', false);
      }
    }, 100);
    log.debug(`The bot is now following ${targetName}`);
  }

  registerCommand('follow', follow, 'Follow a player', 'follow [playername | \'me\']');
}
