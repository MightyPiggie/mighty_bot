const fs = require('fs');
const path = require('path');
const registerCommand = require('./commands.js').registerCommand;
const getUsername = require('./uuidTracker.js').getUsername;
const getUUID = require('./uuidTracker.js').getUUID;

module.exports.inject = inject;

let playTimeInterval;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  const playTimesFile = path.join(__dirname, '../data', 'playTime.json');
  const playTimes = fs.existsSync(playTimesFile) ? JSON.parse(fs.readFileSync(playTimesFile)) : {};

  clearInterval(playTimeInterval);
  playTimeInterval = setInterval(() => {
    Object.values(bot.players).forEach((player) => {
      const uuid = player.uuid;
      if (Object.keys(playTimes).includes(uuid)) {
        playTimes[uuid] += 1;
      } else {
        playTimes[uuid] = 1;
      }
    });

    fs.writeFile(playTimesFile, JSON.stringify(playTimes, null, 4), (err) => {
      if (err) {
        log.err(err);
        throw err;
      }
    });
  }, 60 * 1000);

  /**
   * Get a list of UUIDs, sorted by playtime.
   * The first player has the highest playtime.
   *
   * @return {List} - The list with players.
   */
  function getSortedPlaylist() {
    const playerList = Object.entries(playTimes);
    playerList.sort((id1, id2) => {
      if (id1[1] > id2[1]) {
        return -1;
      }
      if (id1[1] < id2[1]) {
        return 1;
      }
      return 0;
    });
    return playerList;
  }

  /**
   * Get the total playtime in minutes.
   * Either for a single player or for the top players.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function getPlaytime(username, args, response) {
    if (args.length >= 1 && args[0] === 'top') {
      const playerList = getSortedPlaylist();
      const length = playerList.length >= 5 ? 5 : playerList.length;
      let string = '';
      for (let i = 0; i < length; i++) {
        string += `${getUsername(playerList[i][0])}: ${playerList[i][1]}, `;
      }
      response(string);
      return;
    } else {
      const playername = args.length >= 1 ? args[0] : username;
      const uuid = getUUID(playername);

      if (playTimes[uuid] === undefined) {
        response(`I can't find the given player.`);
        return;
      }

      if (args.length === 0) {
        response(`You have played for ${playTimes[uuid]} minutes.`);
      } else {
        response(`${playername} has played for ${playTimes[uuid]} minutes.`);
      }
    }
  }

  registerCommand('playtime', getPlaytime, 'Get the playtime', 'playtime [top/username]');
};
