const registerCommand = require('./commands.js').registerCommand;

module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Jump!
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function jump(username, args, response) {
    bot.setControlState('jump', true);
    bot.setControlState('jump', false);
    log.debug('Made the bot jump.');
  }

  registerCommand('jump', jump, 'Make the bot jump', 'jump');
};
