const fs = require('fs');
const path = require('path');

const onChange = require('on-change');

const registerCommand = require('./commands.js').registerCommand;
const ciCompare = require('../lib/ciCompare.js');

module.exports = {
  inject: inject,
  options: {
    daysTillPhantom: 3,
  },
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  const initialSleepData = fs.existsSync(path.join(__dirname, '../data/', 'sleepReminder.json')) ? require(path.join(__dirname, '../data/', 'sleepReminder.json')) : {};
  const sleepData = onChange(initialSleepData, () => {
    fs.writeFileSync(path.join(__dirname, '../data/', 'sleepReminder.json'), JSON.stringify(sleepData, null, 4));
  });

  let curDay = bot.time.day;
  let done = false;

  setInterval(() => {
    if (curDay != bot.time.day) {
      curDay = bot.time.day;
      done = false;
      for (const player in sleepData) {
        if (Object.prototype.hasOwnProperty.call(sleepData, player) && Object.keys(bot.players).includes(player)) {
          sleepData[player]++;
        }
      }
    }
    if (!done && bot.time.timeOfDay >= 12000) { // When players are able to sleep, send reminder.
      done = true;
      let i = 0;
      for (const player in sleepData) {
        if (sleepData[player] >= bot.options.daysTillPhantom && Object.keys(bot.players).includes(player)) {
          setTimeout(() => { // Set delay between each message as to not spam the server.
            bot.whisper(player, `You haven't slept in ${sleepData[player]} days, you should probably sleep. 'sleep reset' to reset.`);
          }, 1000 * i);
          i++;
        }
      }
    }
  }, 30000);

  /**
   * Handle sleep reminder for Phantoms.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function sleepReminder(username, args, response) {
    if (args.length == 0) {
      if (username in sleepData) {
        response(`You haven't slept in ${sleepData[username]} days. 'sleep reset' to reset.`);
      } else {
        response(`You haven't enabled the sleep reminder. 'sleep enable' to enable.`);
      }
    } else if (ciCompare(args[0], 'enable')) {
      if (username in sleepData) {
        response(`Sleep reminder is already enabled.`);
      } else {
        sleepData[username] = 0;
        response(`Enabled sleep reminder.`);
        log.debug(`${username} enabled his sleep reminder.`);
      }
    } else if (ciCompare(args[0], 'disable')) {
      if (username in sleepData) {
        delete sleepData[username];
        response(`Disabled sleep reminder.`);
        log.debug(`${username} disabled his sleep reminder.`);
      } else {
        response(`Your sleep reminder is already disabled.`);
      }
    } else if (ciCompare(args[0], 'reset')) {
      if (username in sleepData) {
        sleepData[username] = 0;
        response(`Sleep reminder is reset.`);
        log.debug(`${username} reset his sleep reminder.`);
      } else {
        response(`Your sleep reminder is disabled. 'sleep enable' first.`);
      }
    } else {
      response(`Unknown argument for sleep command.`);
    }
  }

  registerCommand('sleep', sleepReminder, 'Handle sleep reminder', 'sleep [enable/disable/reset]');
};
