const fs = require('fs');
const path = require('path');

const vec3 = require('vec3');

const NoteblockSong = require('../lib/noteblock.js');
const registerCommand = require('./commands.js').registerCommand;
const ciCompare = require('../lib/ciCompare.js');

module.exports = {
  inject: inject,
  options: {noteClicksPerTick: 1},
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  let noteblocksTuned = false;
  let noteblocksTuning = false;
  let pianoNoteblocks;
  let bassNoteblocks;
  let snareNoteblocks;
  let guitarNoteblocks;
  let fluteNoteblocks;
  let ticker;

  /**
   * Left click a block.
   *
   * @param {Block} block - The block to left click.
   */
  function leftClickBlock(block) {
    bot._client.write(
        'block_dig', {status: 0, location: block.position, face: 1});
    bot._client.write(
        'block_dig', {status: 1, location: block.position, face: 1});
  }
  /**
   * Right click a block.
   *
   * @param {Block} block - The block to right click.
   */
  function rightClickBlock(block) {
    bot._client.write('block_place', {
      location: block.position,
      direction: 1,
      hand: 0,
      cursorX: 0.5,
      cursorY: 0.5,
      cursorZ: 0.5,
    });
  }

  /**
   * Register and tune the noteblocks.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  async function tune(username, args, response) {
    if (noteblocksTuning) {
      return;
    }
    log.info('Started tuning the noteblocks.');
    noteblocksTuned = false;
    noteblocksTuning = true;

    // Make sure the bot is at the center of the block.
    const centerBlock = bot.blockAt(bot.entity.position.plus(vec3(0, -1, 0)));
    bot.setControlState('sneak', true);
    bot.setControlState('forward', true);
    bot.physics.sneakSpeed = 0.1;
    const lookAtCenter = setInterval(() => {
      bot.lookAt(centerBlock.position.plus(vec3(0.5, 0, 0.5)), true, () => {});
    }, 100);

    await new Promise((resolve) => {
      setTimeout(() => {
        clearInterval(lookAtCenter);
        bot.setControlState('forward', false);
        bot.setControlState('sneak', false);
        bot.physics.sneakSpeed = 0.3;
        resolve();
      }, 2000);
    });

    // Register all the noteblocks.
    pianoNoteblocks = [];
    bassNoteblocks = [];
    snareNoteblocks = [];
    guitarNoteblocks = [];
    fluteNoteblocks = [];

    const noteblocks = bot.findBlocks({matching: (block) => block.name == 'note_block', maxDistance: 20, count: 125});
    noteblocks.forEach((noteblock) => {
      const below = bot.blockAt(noteblock.plus(vec3(0, -1, 0)));
      if (below.name == 'oak_planks') {
        bassNoteblocks.push(bot.blockAt(noteblock));
      } else if (below.name == 'sand' || below.name == 'gravel' || below.name.endsWith('concrete_powder')) {
        snareNoteblocks.push(bot.blockAt(noteblock));
      } else if (below.material == 'wool') {
        guitarNoteblocks.push(bot.blockAt(noteblock));
      } else if (below.name == 'clay') {
        fluteNoteblocks.push(bot.blockAt(noteblock));
      } else {
        pianoNoteblocks.push(bot.blockAt(noteblock));
      }
    });

    // Check whether we have enough noteblocks.
    if (pianoNoteblocks.length < 25) {
      response(`I couldn't find 25 piano noteblocks.`);
    } else if (bassNoteblocks.length < 25) {
      response(`I couldn't find 25 bass noteblocks.`);
    } else if (snareNoteblocks.length < 25) {
      response(`I couldn't find 25 snare noteblocks.`);
    } else if (guitarNoteblocks.length < 25) {
      response(`I couldn't find 25 guitar noteblocks.`);
    } else if (fluteNoteblocks.length < 25) {
      response(`I couldn't find 25 flute noteblocks.`);
    }
    if (pianoNoteblocks.length + bassNoteblocks.length + snareNoteblocks.length + guitarNoteblocks.length + fluteNoteblocks.length < 125) {
      noteblocksTuning = false;
      log.warn(`There aren't enough noteblocks.`);
      return;
    }

    const clicksPerTick = 1;
    let clicks = 0;

    /**
     * Function that waits for a minecraft game tick.
     *
     * @return {Promise} Resolves after a minecraft game tick.
     */
    function waitTick() {
      return new Promise((resolve) => {
        setTimeout(resolve, 1000/20); // 20 game ticks per second
      });
    }

    /**
     * Tune a noteblock to the given pitch.
     *
     * @param {Block} block - The noteblock to tune.
     * @param {interger} pitch - The pitch to tune to.
     */
    async function tuneNoteblock(block, pitch) {
      if (block.name != 'note_block') {
        return;
      }
      let diff = pitch - Math.floor((block.metadata % 50) / 2);
      if (diff < 0) {
        diff += 25;
      }
      for (let i = 0; i < diff; i++) {
        rightClickBlock(block);
        clicks++;
        if (clicks >= clicksPerTick) {
          await waitTick();
          clicks = 0;
        }
      }
    }
    for (let i = 0; i < pianoNoteblocks.length; i++) {
      await tuneNoteblock(pianoNoteblocks[i], i);
    }
    for (let i = 0; i < bassNoteblocks.length; i++) {
      await tuneNoteblock(bassNoteblocks[i], i);
    }
    for (let i = 0; i < snareNoteblocks.length; i++) {
      await tuneNoteblock(snareNoteblocks[i], i);
    }
    for (let i = 0; i < guitarNoteblocks.length; i++) {
      await tuneNoteblock(guitarNoteblocks[i], i);
    }
    for (let i = 0; i < fluteNoteblocks.length; i++) {
      await tuneNoteblock(fluteNoteblocks[i], i);
    }
    noteblocksTuned = true;
    noteblocksTuning = false;
    response('The noteblocks are now tuned.');
    log.debug('The noteblocks are now tuned.');
  }

  /**
   * Play a song.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function play(username, args, response) {
    const songList = fs.readdirSync(path.join(__dirname, '../songs'))
        .filter((file) => file.endsWith('.nbs'))
        .map((song) => song.slice(0, -4));

    if (args.length != 0 && ciCompare(args[0], 'list')) {
      response('I have the following songs: ' + songList.join(', '));
      log.debug('Gave out the songs list.');
      return;
    } else if (args[0] == 'nothing') {
      clearInterval(ticker);
      log.debug('Stopped playing music.');
      return;
    } else if (songList.length == 0) {
      response('I don\'t have any songs yet.');
      return;
    } else if (!noteblocksTuned) {
      response('The noteblocks aren\'t tuned yet.');
      return;
    }

    let song;
    if (args.length == 0) {
      song = new NoteblockSong(
          path.join(__dirname, '../songs', songList[0] + '.nbs'));
    } else if (songList.includes(args[0].toLowerCase())) {
      song = new NoteblockSong(path.join(
          __dirname, '../songs',
          songList[songList.indexOf(args[0].toLowerCase())] + '.nbs'));
    } else {
      response('I don\'t know that song.');
      return;
    }

    let alreadyLooking = false;

    /**
     * Look at the given block.
     *
     * @param {Block} block - The block to look at.
     */
    function lookAt(block) {
      if (!alreadyLooking) {
        bot.lookAt(block.position, true, () => {});
        alreadyLooking = true;
      }
    }

    let currentTick = 0;
    let arm = 'right';
    let jumpsLeft = song.notes[currentTick].jumps;
    clearInterval(ticker);
    ticker = setInterval(() => {
      jumpsLeft--;
      if (jumpsLeft <= 0) {
        bot.swingArm(arm);
        arm = arm == 'right' ? 'left' : 'right';
        song.notes[currentTick].layers.forEach((note) => {
          if (note.instrument == 0) {
            leftClickBlock(pianoNoteblocks[note.pitch - 33]);
            lookAt(pianoNoteblocks[note.pitch - 33]);
          } else if (note.instrument == 1 || note.instrument == 2 || note.instrument == 4) {
            leftClickBlock(bassNoteblocks[note.pitch - 33]);
            lookAt(bassNoteblocks[note.pitch - 33]);
          } else if (note.instrument == 3) {
            leftClickBlock(snareNoteblocks[note.pitch - 33]);
            lookAt(snareNoteblocks[note.pitch - 33]);
          } else if (note.instrument == 5) {
            leftClickBlock(guitarNoteblocks[note.pitch - 33]);
            lookAt(guitarNoteblocks[note.pitch - 33]);
          } else if (note.instrument == 6) {
            leftClickBlock(fluteNoteblocks[note.pitch - 33]);
            lookAt(fluteNoteblocks[note.pitch - 33]);
          }
        });
        currentTick++;
        if (song.notes[currentTick] == undefined) {
          clearInterval(ticker);
        } else {
          jumpsLeft = song.notes[currentTick].jumps;
        }
        alreadyLooking = false;
      }
    }, 1000 / (song.tempo / 100));
    log.info(`${username} made the bot play ${args[0]}`);
  }

  registerCommand('play', play, 'Play a song', 'play [song name]');
  registerCommand('tune', tune, 'Register and tune the noteblocks', 'tune');
};
