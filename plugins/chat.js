const registerCommand = require('./commands.js').registerCommand;

module.exports.inject = inject;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Make the bot say something.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function say(username, args, response) {
    bot.chat(args.join(' '));
    log.debug(`${username} made the bot say: '${args.join(' ')}'`);
  }

  registerCommand('say', say, 'Make the bot say something', 'say text', 1);
};
