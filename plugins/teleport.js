const registerCommand = require('./commands.js').registerCommand;

module.exports = {
  inject: inject,
};

const buttonTypes = [
  'stone_button',
  'oak_button',
  'spruce_button',
  'birch_button',
  'jungle_button',
  'acacia_button',
  'dark_oak_button',
  'mangrove_button',
  'cherry_button',
  'bamboo_button',
  'crimson_button',
  'warped_button',
  'polished_blackstone_button',
];

const signTypes = [
  'oak_sign',
  'spruce_sign',
  'birch_sign',
  'jungle_sign',
  'acacia_sign',
  'dark_oak_sign',
  'mangrove_sign',
  'cherry_sign',
  'bamboo_sign',
  'crimson_sign',
  'warped_sign',
  'oak_wall_sign',
  'spruce_wall_sign',
  'birch_wall_sign',
  'jungle_wall_sign',
  'acacia_wall_sign',
  'dark_oak_wall_sign',
  'mangrove_wall_sign',
  'cherry_wall_sign',
  'bamboo_wall_sign',
  'crimson_wall_sign',
  'warped_wall_sign',
];

/**
 * Check if a button has a sign with the username above or below it
 *
 * @param {Bot} bot - The bot
 * @param {Vec3} button - The location of the button
 * @param {string} username - The username to search for
 * @return {bool} - True if button has a corresponding sign
 */
function correctButton(bot, button, username) {
  const blockAbove = bot.blockAt(button.offset(0, 1, 0));
  if (signTypes.includes(blockAbove.name)) {
    if (blockAbove.signText.toLowerCase().includes(username.toLowerCase())) {
      return true;
    }
  }
  const blockBelow = bot.blockAt(button.offset(0, -1, 0));
  if (signTypes.includes(blockBelow.name)) {
    if (blockBelow.signText.toLowerCase().includes(username.toLowerCase())) {
      return true;
    }
  }
  return false;
}

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Teleport you to the bot if there is a button with sign nearby.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function teleport(username, args, response) {
    const buttons = bot.findBlocks({
      matching: (block) => buttonTypes.includes(block.name),
      maxDistance: 10,
      count: 10,
    });

    for (const button of buttons) {
      if (correctButton(bot, button, username)) {
        response('Teleporting you now...');
        bot.activateBlock(bot.blockAt(button, false));
        return;
      }
    }

    response('I don\'t have a button with your name here.');
  }

  registerCommand('tp', teleport, 'Teleport to the bot', 'teleport');
};
