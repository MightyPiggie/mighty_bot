const fs = require('fs');
const path = require('path');

module.exports.inject = inject;

let playerCountInterval;

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  const csvPath = path.join(__dirname, '../data', 'playerCount.csv');
  if (!fs.existsSync(csvPath)) {
    fs.writeFile(csvPath, 'time,count\n', (error) => {
      if (error) throw error;
      log.info(`playerCount.csv didn't excist, creating one`);
    });
  }

  const recordPlayerCount = () => {
    const date = new Date();
    fs.appendFile(csvPath, `"${('0' + date.getDate()).slice(-2)}-${('0' + (date.getMonth() + 1)).slice(-2)} ${date.getHours()}:${date.getMinutes()}",${Object.keys(bot.players).length}\n`, (error) => {
      if (error) throw error;
    });
  };

  clearInterval(playerCountInterval);
  playerCountInterval = setInterval(recordPlayerCount, 600000);
};
