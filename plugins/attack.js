const registerCommand = require('./commands.js').registerCommand;
const ciCompare = require('../lib/ciCompare.js');

module.exports = {
  inject: inject,
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  let attackInterval;
  let target = '';

  /**
   * Async attack a mob
   */
  function attack() {
    const entity = bot.nearestEntity((entity) => {
      return ciCompare(entity.name, target);
    });
    if (entity) {
      bot.attack(entity);
    }
  }

  /**
   * Attack a mob
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function attackCommand(username, args, response) {
    if (args.length != 0 && (ciCompare(args[0], 'stop'))) {
      clearInterval(attackInterval);
      response('Canceled attack');
    } else if (args.length != 0) {
      target = args[0];
      attackInterval = setInterval(attack, 1000);
      response(`Started attacking ${target}`);
    }
  }
  registerCommand('attack', attackCommand, 'Attack monsters.', 'monster to be attacked', 1);
};
