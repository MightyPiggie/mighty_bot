const fs = require('fs');
const path = require('path');
const onChange = require('on-change');

module.exports = {inject, getUUID, getUsername};

const UUIDsFile = path.join(__dirname, '../data/uuids.json');
const initialUUIDs = fs.existsSync(UUIDsFile) ? require(UUIDsFile) : {};
const UUIDs = onChange(initialUUIDs, (path, value, previousValue) => {
  fs.writeFileSync(UUIDsFile, JSON.stringify(UUIDs, null, 4));
});

/**
 * Update the username of a UUID.
 *
 * @param {string} uuid - The uuid.
 * @param {string} username - The username.
 */
function updateUUID(uuid, username) {
  UUIDs[uuid] = username;
};

/** Get the UUID of a username.
 *
 * @param {string} username - The username.
 * @return {string} - The UUID or undefined if not found.
 */
function getUUID(username) {
  if (username === undefined) {
    return undefined;
  }

  for (const uuid in UUIDs) {
    if (UUIDs[uuid].toLowerCase() === username.toLowerCase()) {
      return uuid;
    }
  }
  return undefined;
};

/** Get the username of a UUID.
 *
 * @param {string} uuid - The UUID.
 * @return {string} - The username or undefined if not found.
 */
function getUsername(uuid) {
  return UUIDs[uuid];
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  bot.on('playerJoined', (player) => {
    updateUUID(player.uuid, player.username);
  });

  for (const player of Object.values(bot.players)) {
    updateUUID(player.uuid, player.username);
  }
};
