const registerCommand = require('./commands.js').registerCommand;

module.exports.inject = inject;

/**
 * Get a random interger.
 *
 * Get a random interger between 0 and max.
 *
 * @param {int} max - The maximum interger.
 * @return {int} - The random interger.
 */
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  /**
   * Flip a coin.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function coinFlip(username, args, response) {
    response(`You flipped a coin, it was ${getRandomInt(2) ? 'heads' : 'tails'}!`);
  }

  /**
   * Roll a dice.
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function diceRoll(username, args, response) {
    let sides = 6;
    if (args.length != 0) {
      try {
        sides = parseInt(args[0]);
      } catch (e) {
        response('Received invalid number.');
        log.debug(`Dice called with invalid number: ${args[0]}`);
        return;
      }
    }
    const result = getRandomInt(sides) + 1;
    response(`You rolled a ${sides} sided dice, it is ${result}!`);
  }

  registerCommand('coinflip', coinFlip, 'Flip a coin', 'coinflip');
  registerCommand('dice', diceRoll, 'Roll a dice', 'dice [sides]');
};
