const mineflayer = require('mineflayer');
const tpsPlugin = require('mineflayer-tps')(mineflayer);

const registerCommand = require('./commands.js').registerCommand;

module.exports = {
  inject: inject,
};

/**
 * Inject this plugin into the bot.
 *
 * @param {Bot} bot - The bot to inject into.
 */
function inject(bot) {
  bot.loadPlugin(tpsPlugin);

  /**
   * Check current Ticks Per Second
   *
   * @param {string} username - The user that triggered the command.
   * @param {string[]} args - The given arguments.
   * @param {function} response - The response function.
   */
  function checkTps(username, args, response) {
    const tps = bot.getTps();
    response(`Current TPS: ${tps}, which is ${tps*5}% of normal speed.`);
  }

  registerCommand('tps', checkTps, 'Check Ticks Per Second', 'tps');
};
