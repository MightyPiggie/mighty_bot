const fs = require('fs');
const path = require('path');

const stripAnsi = require('strip-ansi');

/**
 * Simple logger class.
 */
class Logger {
  /** Constructor for Logger class.
   *
   * @param {string} logLevel - Log lever of the logger.
   */
  constructor(logLevel = 'debug') {
    this.dir = path.join(__dirname, '../log');
    if (!fs.existsSync(this.dir)) {
      fs.mkdirSync(this.dir);
    }
    this.logLevels = {'error': 0, 'warn': 1, 'info': 2, 'debug': 3};
    if (!Object.prototype.hasOwnProperty.call(this.logLevels, logLevel)) {
      throw new Error('Given log level is not a valid log level.');
    }
    this.logLevel = this.logLevels[logLevel];
  }

  /**
   * Log an error.
   *
   * @param {string} string - String to log.
   */
  error(string) {
    string += '\x1b[0m';
    this.log('\x1b[31m[ERROR]', string, 0);
  }

  /**
   * Log a warning.
   *
   * @param {string} string - String to log.
   */
  warn(string) {
    string += '\x1b[0m';
    this.log('\x1b[33m[WARN] ', string, 1);
  }

  /**
   * Log info.
   *
   * @param {string} string - String to log.
   */
  info(string) {
    string += '\x1b[0m';
    this.log('\x1b[32m[INFO] ', string, 2);
  }

  /**
   * Log debug.
   *
   * @param {string} string - String to log.
   */
  debug(string) {
    string += '\x1b[0m';
    this.log('\x1b[34m[DEBUG]', string, 3);
  }

  /**
   * The main log command.
   *
   * @param {string} prefix - The prefix before the log.
   * @param {string} string - The string to log.
   * @param {string} logLevel - The log level.
   */
  log(prefix, string, logLevel) {
    const date = new Date();
    string = prefix + '(' + date.toLocaleString() + '): ' + string;

    const logFile = fs.createWriteStream(path.join(this.dir, 'log_' + date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2) + '.txt'), {flags: 'a'});
    logFile.write(stripAnsi(string) + '\n');
    if (logLevel <= this.logLevel) {
      console.log(string);
    }
  }
}

global.log = new Logger('debug');
