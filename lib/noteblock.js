const fs = require('fs');

/**
 * Class for a single note.
 */
class Note {
  /**
   * Constructor for Note class.
   *
   * @param {integer} instrument - Instrument ID.
   * @param {integer} pitch - Pitch of the note.
   */
  constructor(instrument, pitch) {
    this.instrument = instrument;
    this.pitch = pitch;
  }
}

/**
 * Class for a single tick in a noteblock song.
 */
class NoteTick {
  /**
   * Constructor for the NoteTick class.
   *
   * @param {integer} jumps - The number of jumps from the previous tick.
   * @param {Note[]} layers - The notes in this tick.
   */
  constructor(jumps, layers) {
    this.jumps = jumps;
    this.layers = layers;
  }
}

/**
 * Class for a complete noteblock song.
 */
class NoteblockSong {
  /**
   * The constructor for the NoteblockSong class.
   *
   * @param {string} filename - The filename of the *.nbs to read.
   */
  constructor(filename) {
    const fd = fs.openSync(filename, 'r');

    // Legacy old format length
    readShort(fd);

    // NBS version
    this.version = readByte(fd);

    // Vanilla instrument count
    this.instrumentCount = readByte(fd);

    // Song length
    this.length = readShort(fd);

    // Layer count
    this.layerCount = readShort(fd);

    // Song Name
    this.name = readString(fd);

    // Song Author
    this.Author = readString(fd);

    // Song original author
    this.originalAuthor = readString(fd);

    // Song description
    this.description = readString(fd);

    // Song tempo
    this.tempo = readShort(fd);

    // Auto-saving
    readByte(fd);
    // Auto-saving duration
    readByte(fd);

    // Time signature
    this.timeSignature = readByte(fd);

    // Minutes spent
    readInt(fd);
    // Left clicks
    readInt(fd);
    // Right clicks
    readInt(fd);
    // Noteblocks added
    readInt(fd);
    // Noteblocks removed
    readInt(fd);

    // MIDI/Schematic file name
    this.midiName = readString(fd);

    // Loop
    this.loop = readByte(fd);

    // Max loop count
    this.maxLoop = readByte(fd);

    // Loop start tick
    this.loopStart = readShort(fd);

    this.notes = [];
    let nextTick = readShort(fd);
    while (nextTick != 0) {
      this.notes.push(new NoteTick(nextTick, readTick(fd)));
      nextTick = readShort(fd);
    }
  }
}

/**
 * Read a single byte from the file descriptor,
 * and return it as an integer.
 *
 * @param {integer} file - The file descriptor.
 * @return {integer} The read number.
 */
function readByte(file) {
  const buffer = Buffer.alloc(1);
  fs.readSync(file, buffer, 0, 1, null);
  return buffer.readInt8();
}

/**
 * Read a single short from the file descriptor,
 * and return it as an integer.
 *
 * @param {integer} file - The file descriptor.
 * @return {integer} The read number.
 */
function readShort(file) {
  const buffer = Buffer.alloc(2);
  fs.readSync(file, buffer, 0, 2, null);
  return buffer.readInt16LE();
}

/**
 * Read a single integer from the file descriptor,
 * and return it as an integer.
 *
 * @param {integer} file - The file descriptor.
 * @return {integer} The read number.
 */
function readInt(file) {
  const buffer = Buffer.alloc(4);
  fs.readSync(file, buffer, 0, 4, null);
  return buffer.readInt32LE();
}

/**
 * Read a single string from the file descriptor,
 * and return it as an string.
 *
 * @param {integer} file - The file descriptor.
 * @return {string} The read string.
 */
function readString(file) {
  const size = readInt(file);
  let string = '';
  Array(size).fill().forEach(() => {
    string += String.fromCharCode(readByte(file));
  });
  return string;
}

/**
 * Read a single note from the file descriptor,
 * and return it as a note class.
 *
 * @param {integer} file - The file descriptor.
 * @return {Note} The read note.
 */
function readNote(file) {
  const note = new Note(readByte(file), readByte(file));
  readByte(file);
  readByte(file);
  readShort(file);
  return note;
}

/**
 * Read a single note tick from the file descriptor,
 * and return it as a note tick class.
 *
 * @param {integer} file - The file descriptor.
 * @return {NoteTick} The read note tick.
 */
function readTick(file) {
  const layers = [];
  let nextLayer = readShort(file);
  while (nextLayer != 0) {
    layers.push(readNote(file));
    nextLayer = readShort(file);
  }
  return layers;
}

module.exports = NoteblockSong;
