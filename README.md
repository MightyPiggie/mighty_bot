# Mighty_Bot

This is a bot made using [Mineflayer](https://github.com/PrismarineJS/mineflayer) and inspired by the [Helperbot](https://github.com/Darthfett/helperbot).  
It is a general purpose bot designed to improve the vanilla survival multiplayer experience.

## How to use

First you have to create a `login.json` that contains the login parameters of your account.  
Put this file in the `data` folder.

```json
{
    "username": "username",
    "password": "password"
}
```
Then you can start the bot by using `node mighty_bot.js hostname[:port]`

You can also put the `host` and the optional `port` inside the `login.json`

If you want to change some of the default options, you have to run it at least once.  
After that you will find a `options.json` inside the `data` folder in which you can change the options.

## Plugins

Plugins are (small) components that are loaded into the bot. Plugins are in the `plugins` folder, and need to export an `inject` function.  
If your plugins needs persistant options, you can also export and `options` object containing all the options with their default value.  
Plugins can be disabled by adding the name of the plugin to the `disabled` array in `data/options.json`

| Name | Description | Dependencies |
|---|---|---|
| `attack` | Attack mobs every second | `commands` |
| `baseFinder` | This plugin can help people find each others bases. | `commands`, `uuidTracker` |
| `calculator` | Calculate math expressions. | `commands` |
| `chat` | Make the bot say something. | `commands` |
| `commands` | Register commands that people are able to call. |   |
| `follow` | Make the bot able to follow a player. | `commands` |
| `formatting` | Show the Minecraft formatting codes. | `commands` |
| `health` | Health management of the bot like eating and damage warning. |   |
| `help` | Add help menu for commands. | `commands` |
| `jokes` | Tell a random joke. | `commands` |
| `jump` | Make the bot jump when called to do so. | `commands` |
| `mine` | Make the bot mine blocks. | `commands` |
| `music` | Play songs on noteblocks. | `commands` |
| `permissions` | Add permissions to the bot. | `commands` |
| `playerCount` | Log the player count every 10 minutes. | |
| `playTime` | Keep track of the playtime of all the players. | `commands`, `uuidTracker` |
| `random` | Flip a coin or roll a dice. | `commands` |
| `skinBlinker` | Blink the skin layers of the bot. | |
| `sleepReminder` | Remind you to sleep to prevent Phantoms. | `commands` |
| `teleport` | Teleport you to the bot via ender-porter. | `commands` |
| `time` | Tells you the time and moon phase. | `commands` |
| `tps` | Tells you the TPS. | `commands` |
| `uuidTracker` | Keep track of peoples UUID locally. | |
| `welcome` | Welcome new players | |
| `wiki` | Look up an Minecraft wiki page. | `commands` |

### Commands plugin

By default, the bot will require that you call commands by using his name as a prefix.  
So if you bots name is `Obi_Wan` you have to call the commands by using `Obi_Wan` as a prefix: `Obi_Wan say Hello there` (case insensitive)  
Commands can also by called by whispering to the bot, then you never have to use a prefix.

### Music plugin

The music plugin is able to play `.nbs` songs. These songs can be downloaded from [opennbs.org/songs/](https://opennbs.org/songs/) or created yourself by using the [Open Note Block Studio](https://opennbs.org/).  
Open Note Block Studio is also able to convert MIDI files into nbs files.

These songs can be put into the `songs` folder (Please don't use spaces in the name)  
To play a song, to bot first has to find and tune the noteblocks using `tune`  
When you tune, the bot will first center itself and then it will try to find all the noteblocks around him and figure out which instrument they are.  
You have to make sure the bot is able to find 25 noteblocks of the following instruments: `piano (air), bass (wood), snare (sand/gravel), guitar (wool) and flute (clay)` otherwise it will not work.  

And once all the noteblocks are registered and tuned you can play a song by using the `play [songname]` command.  

### Base Finder plugin

The base finder plugin is designed so that people can know the location of other people, without having to ask them. (So even when they are offline)  
This is useful on vanilla PvE servers where there is lots of interaction between the community.  
It is also specifically designed to work nicely on servers with a nether tunnel network.

A player can register their base by using `setbase x_cordinate y_cordinate` (The cordinates in the overworld)  
Then, a player is able to look up somebodies base location by using `where [is] playername`  
You will then get a response like: `playername's base is at X:800,Y:100 in the overworld, and X:100,Y:12 in the nether, which should be the east tunnel.`  
### Permissions plugin

The permissions plugin allows you to specify a few select people in the `masters` array in the `options.json` who are allowed to interact with your bot.  
To get a list of current masters, call `masters`  
You can add somebody to the list with `master add playername` and remove with `master remove playername`  
The first person needs to be added manually to the masters.json.
The `masters` array needs to have the following format:

```json
[
    "player1",
    "player2"
]
```

If `options.json` doesn't exist yet, simply run the bot once to generate the file.
